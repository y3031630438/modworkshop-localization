<?php

//This is only for slavic languages at the moment as their spelling changes in certain cases requiring us to add an additional "plural".

$l['year2'] = 'lata'; 
$l['months2'] = 'miesiące'; 
$l['days2'] = 'dni';
$l['weeks2'] = 'tygodnie';
$l['hours2'] = 'godziny';
$l['minutes2'] = 'minuty';
$l['seconds2'] = 'sekundy';
