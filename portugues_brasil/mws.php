<?php

$l['mydownloads_cancel'] = 'Cancelar';
$l['mydownloads_cant_edit_comment'] = 'Você precisar esperar {1} segundos antes de poder editar';
$l['mydownloads_categories'] = "Categorias";
$l['mydownloads_category'] = 'Categoria';
$l['mydownloads_collaborating'] = 'Colaborando';
$l['mydownloads_date'] = 'Data';
$l['mydownloads_delete'] = 'Excluir';
$l['mydownloads_downloads'] = 'Baixados';
$l['mydownloads_downloads_number'] = "{1} mod(s)";
$l['mydownloads_download_collaborator'] = 'Colaborador';
$l['mydownloads_download_delete_comment'] = "Excluir";
$l['mydownloads_download_edit_comment'] = "Editar";
$l['mydownloads_download_name'] = "Nome";
$l['mydownloads_download_views'] = "Visualizações";
$l['mydownloads_edit'] = 'Editar Mod';
$l['mydownloads_editing_download'] = 'Editar';
$l['mydownloads_empty_reason'] = 'Você deve específicar um motivo de estar denunciando o mod selecionado.';
$l['mydownloads_enter_a_comment'] = 'Coloque um comentário por favor.';
$l['mydownloads_error'] = 'Erro';
$l['mydownloads_error_attachsize'] = "O arquivo que você enviou é muito grande. O tamanho máximo para esse tipo de arquivo é {1} kilobytes.";
$l['mydownloads_error_attachtype'] = "O tipo de arquivo que você anexou não é permitido. Use outro tipo de arquivo.";
$l['mydownloads_error_invalidrating'] = "Você selecionou uma avaliação inválida para esse mod.";
$l['mydownloads_error_uploadfailed'] = "Fracassado em enviar o arquivo. Por favor escolhe um arquivo válido e tente novamente.";
$l['mydownloads_error_uploadfailed_detail'] = "Detalhes do erro: ";
$l['mydownloads_error_uploadfailed_lost'] = "O anexado não pode ser achado no servidor.";
$l['mydownloads_error_uploadfailed_movefailed'] = "Teve um problema de mover o arquivo enviado para o seu destino.";
$l['mydownloads_error_uploadfailed_nothingtomove'] = "Um arquivo inválido estava específicado, o arquivo enviado não pôde ser movido para o seu destino.";
$l['mydownloads_error_uploadfailed_php1'] = "PHP retornado: O arquivo enviado passou do upload_max_filesize diretório em php.ini. Por favor contate o seu administrador do fórum sobre esse erro.";
$l['mydownloads_error_uploadfailed_php2'] = "O arquivo enviado passou do tamanho máximo específicado.";
$l['mydownloads_error_uploadfailed_php3'] = "O arquivo enviado só teve uma parte enviada.";
$l['mydownloads_error_uploadfailed_php4'] = "Nenhum arquivo foi enviado.";
$l['mydownloads_error_uploadfailed_php6'] = "PHP retornado: Uma pasta tempoária faltando. Por favor contate o seu administrador do fórum sobre esse erro.";
$l['mydownloads_error_uploadfailed_php7'] = "PHP retornado: Fracassado em escrever o arquivo para o disco. Por favor contate o seu administrador do fórum sobre esse erro.";
$l['mydownloads_error_uploadfailed_phpx'] = "PHP código de erro retornado: {1}. Por favor contate o seu administrador do fórum sobre esse erro.";
$l['mydownloads_error_uploadsize'] = "O tamanho do arquivo enviado é muito grande.";
$l['mydownloads_exceeded'] = 'limite de envio do PHP alcançado. Máximo é {1}.';
$l['mydownloads_author'] = 'Autor';
$l['mydownloads_flood_check'] = "Você tá tentando postar um comentário muito rápido depois de ter postado a anterior. Por favor espere mais {1} mais segundo(s)."; // copied from MyBB :P
$l['mydownloads_hidden'] = 'Escondido';
$l['mydownloads_history'] = 'Histórico de Download';
$l['mydownloads_invalid_banner'] = 'Você enviou uma capa inválida. Extensões válidas: gif, png, jpeg, apng e webp.';
$l['mydownloads_non_secure_banner'] = 'Você enviou uma capa que não é fornecido com HTTPS. Nosso site só usa HTTPS. Por favor coloque uma capa que usa HTTPS.';
$l['mydownloads_invalid_extension'] = 'A imagem só pode ter uma das extensões: jpeg, png, webp, apng, ou gif.';
$l['mydownloads_invalid_url'] = 'O URL seguinte é inválido: ';
$l['mydownloads_invite_only'] = "Apenas Convidados";
$l['mydownloads_last_updated'] = 'Última Vez Atualizado';
$l['mydownloads_log_in_register'] = "Para deixar um comentário você deve logar.";
$l['mydownloads_max_height'] = 'A altura da sua imagem é mais alta do que {1}px.';
$l['mydownloads_max_files'] = 'Você pode enviar no máximo {1} arquivos para cada mod.';
$l['mydownloads_max_previews'] = 'Você pode enviar no máximo {1} imagens para cada mod.';
$l['mydownloads_max_files_error'] = 'Você não pode enviar mais arquivos porque você alcançou o seu limite desse mod.';
$l['mydownloads_max_previews_error'] = 'Você não pode enviar mais imagens porque você alcançou o seu limite desse mod.';
$l['mydownloads_max_res'] = 'A suas imagens são limitadas para {1}px de largura e {2}px de altura.';
$l['mydownloads_max_width'] = 'A largura da sua imagem é mais alta do que {1}px.';
$l['mydownloads_awaiting_approval'] ='Aprovação Pendente';
$l['mydownloads_new_comment'] = '{1} postou um comentário no seu mod {2}';
$l['mydownloads_not_invited'] = "Esse mod \"{1}\" está apenas para convidados.<br/>Você precisa ser convidado para ter o acesso. Para ter o acesso contate \"{2}\".";
$l['mydownloads_no_cid'] = "A categoria que você selecionou não é válida.";
$l['mydownloads_no_description'] = "Você ainda não fez uma descrição.";
$l['mydownloads_no_did'] = "O mod que você selecionou não é válida.";
$l['mydownloads_no_fid'] = "O arquivo que você selecionou não é válida.";
$l['mydownloads_no_dl_name'] = "Você ainda não colocou um nome pro arquivo.";
$l['mydownloads_no_mod_name'] = "Você ainda não colocou um nome pro mod.";
$l['mydownloads_no_dl_type'] = "Você ainda não selecionou uma categoria de arquivo.";
$l['mydownloads_desc_too_long'] = 'A descrição não pode ser mais longa do que 220 letras.';
$l['mydownloads_no_dl_visibility'] = 'Você selecionou um valor de visibilidade inválida.';
$l['mydownloads_no_permissions'] = "Você não tem permissão para fazer essa ação.";
$l['mydownloads_no_files'] = 'Sem Arquivos';
$l['mydownloads_number_downloads'] = "Mais Baixados";
$l['mydownloads_options'] = 'Opções';
$l['mydownloads_preview_empty'] = 'O campo de imagem está vazia.';
$l['mydownloads_rate_banned'] = "Você não pode avaliar enquanto você estiver banido.";
$l['mydownloads_rating_too_fast'] = 'Você só pode avaliar uma vez cada {1} segundos. Tente novamente em {2} segundo(s).';
$l['mydownloads_select_category'] = 'Seleciona uma categoria.';
$l['mydownloads_sortby'] = 'Ordenar por';
$l['mydownloads_submit'] = 'Enviar';
$l['mydownloads_upload_mod'] = 'Enviar um mod';
$l['mydownloads_download_changelog'] = 'Notas de Mudanças';
$l['mydownloads_download_description'] = 'Descrição';
$l['mydownloads_no_description'] = 'Sem descrição.';
$l['mydownloads_no_name'] = 'Sem Nome';
$l['mydownloads_version'] = 'Versão';
$l['mydownloads_sub_categories'] = 'Subcategorias';
$l['mydownloads_suspended'] = "Suspenso";
$l['mydownloads_suspended_error'] = "Esse mod está suspenso e atualmente não pode ser acessado.";
$l['mydownloads_tags'] = 'Tags';
$l['mydownloads_unlisted'] = "Não listado";
$l['mydownloads_upload_problem_pr_already_exists'] = "Uma imagem com o mesmo nome já foi enviada";
$l['mydownloads_main'] = 'Principal';
$l['mydownloads_patch'] = 'Patch';
$l['mydownloads_addon'] = 'Complemento';
$l['mydownloads_edited'] = "(Editado)";
$l['mydownloads_comment_mention'] = "Você foi mencionado em um comentário postado por {1} no mod {2}";
$l['mydownloads_guests_cannot_submit'] = 'Para enviar mods você deve logar em nosso site. <a href="/login">Logar atráves do Steam</a>';
$l['mydownloads_something_wrong'] = "Tem algo de errado";
$l['mydownloads_bad_category'] = "Você não tem permissões para enviar nessa categoria";
$l['mods_game'] = "Jogo";
$l['mods_games'] = "Jogos";
$l['mods_for'] = "{1} Mods";
$l['mods'] = "Mods";
$l['profile_pm'] = "Mensagem Privada";
$l['report'] = "Denunciar";
$l['report_desc'] = '
Se você acha que {1} está quebrando as nossas <a href="/rules">regras</a>, você pode denunciar isso para gente e um moderador irá checar assim que possível
<br>
Denuncia em massa ou por razão boba não vai ser tolerado!
';
$l['no_perms'] = "Você não tem permissão para fazer essa ação";
$l['submit'] = "Enviar";
$l['images'] = 'Imagens';
$l['image'] = 'Imagem';
$l['files'] = 'Arquivos';
$l['none'] = 'Nada';
$l['tab_main'] = 'Principal';
$l['tab_extra'] = 'Extra';
$l['type'] = 'Tipo';
$l['warning'] = 'AVISO!';
$l['wiki'] = 'Wiki';
$l['discord'] = 'Discord';
$l['forums'] = 'Fóruns';
$l['support_us'] = 'Apoie a gente';
$l['support_mws'] = 'Apoie a ModWorkshop';
$l['more'] = 'Mais';
$l['steam_group'] = 'Grupo do Steam';
$l['steam_profile'] = 'Perfil do Steam';
$l['rules'] = 'Regras';
$l['conduct'] = 'Código de Conduta';
$l['privacy'] = 'Política de Privacidade';
$l['terms'] = 'Termos de Serviço';
$l['about'] = 'Sobre nós';
$l['about_mws'] = 'Sobre ModWorkshop';
$l['about_desc'] = "
ModWorkshop é uma plataforma que tem um foco de mods para jogos, providênciando as ferramentas necessárias para compartilhar e criar mods, ferramentas e idéias. 

Nós começamos em 2013 como LastBullet, o número um em mods do Payday.
Com passar do tempo, a gente re inventou como ModWorkshop. Nós queriamos expandir do que ter apenas mods do Payday. 
Agora, nós temos mod para múltiplos jogos que nem PAYDAY 2, PAYDAY: The Heist, Noita, Enter the Gungeon e mais!

Nós achamos que os mods devem ter o código aberto, não fechado. Não tem nenhuma razão em fazer mods se os outros não estiverem disponíveis para outras pessoas aprenderem juntas. 
Que nem, nós temos uma regra onde que os mods precisam ter um código-aberto até uma certa altura.

Nós amamos modificar e se você enviar um mod para algum jogo, sinta-se livre em fazer isso!
Se a seção '[Outros Jogos](/game/other)' não for suficiente, contate a gente e nós vamos discutir sobre fazer uma nova seção dedicada para esse jogo!";
//
$l['my_profile'] = 'Perfil';
$l['user_settings'] = "Configurações de Usuários";
$l['logout'] = "Deslogar";
$l['last_updates_games'] = "Jogos Recém Atualizados";
$l['game_category'] = "Jogo/Categoria";
$l['likes'] = "Mais Curtidas";
$l['no_mods_found'] = "Sem mods achados";
$l['no_more_mods_found'] = "Não achamos mais mods";
$l['no_users_found'] = "Sem usuários achados";
$l['no_more_users_found'] = "Não achamos mais usuários";
$l['load_more'] = "Carregar Mais...";
$l['management'] = "Gerenciador";
$l['moderators'] = "Moderadores";
$l['translators'] = "Tradutores do Site";
$l['search_results'] = "Resultados da Pesquisa";
$l['search'] = "Pesquisa";
$l['section_admins'] = "Adminstradores das Seções";
$l['user'] = "Usuário";
$l['mod'] = "Mod";
$l['view_profile'] = "Visualizar Perfil";
$l['view_steam_profile'] = "Visualizar o Perfil Steam";
$l['home'] = 'Home';
$l['all'] = 'Tudo';
$l['publish_date'] = 'Data de Publicação';
$l['ie_warn'] = "Nós detectamos que você está rodando em Internet Explorer, o nosso site não tem suporte de IE e você vai ter problemas com isso. É fortemente recomendado que você mude para outro navegador mais moderno! ";
$l['return_to_top'] = "Retornar para o topo";
$l['powered_by_mybb'] = 'Powered by <a href="https://mybb.com" target="_blank">MyBB</a>.';
$l['powered_by_steam'] = 'Login Powered By <a href="https://steampowered.com" target="_blank">Steam</a>.';
$l['search_mods_global'] = 'Pesquisar Mods Globalmente';
$l['search_mods_cat'] = 'Pesquisar Mods em categoria';
$l['search_mods_game'] = 'Pesquisar Mods em Jogo';
$l['search_user'] = 'Pesquisar Usuário';
$l['search_users'] = 'Pesquisar Usuário(s)';
$l['comments_disabled'] = 'Comentários foi desativado. Apenas os donos do mod e os moderadores podem comentar.';
$l['files_waiting'] = 'Arquivos com Aprovação Pedente';
$l['file_waiting'] = 'Esperando por Aprovação';
$l['file_approved'] = 'Aprovado';
$l['popular_now'] = 'Popular Agora';
$l['banner'] = 'Capa';
$l['saving'] = 'Salvando...';
$l['save'] = 'Salvar';
$l['saved'] = 'Salvado!';
$l['search_for_user'] = 'Pesquisar por um usuário...';
$l['site_desc'] = 'ModWorkshop é uma plataforma que hosteia mods para jogos, providênciando as ferramentas necessárias para compartilhar mods, ferramentas e idéias.';
$l['liked_mods'] = 'Mods Curtidos';
$l['pin'] = 'Fixar';
$l['unpin'] = 'Desfixar';
$l['pinned'] = 'Fixado';
$l['pin_confirm'] = 'Tem certeza que você quer fixar esse comentário?';
$l['unpin_confirm'] = 'Tem certeza que você quer desfixar esse comentário?';
$l['follow'] = 'Seguir';
$l['follow_cat_help'] = 'Seguir essa categoria para ver os seus mods em seus mods seguidos';
$l['follow_game_help'] = 'Seguir esse jogo para ver seus mods em seus mods seguidos';
$l['follow_user_help'] = 'Seguir esse usuário para ver os mods dele(a) em seus mods seguidos';
$l['unfollow'] = 'Para de seguir';
$l['subscribe'] = 'Inscreva-se';
$l['unsubscribe'] = 'Desinscreve';
$l['followed'] = 'Mods Seguidos';
$l['browse_all_messages'] = 'Navegar em todas as mensagens';
$l['are_you_sure_undone'] = 'Tem certeza que deseja fazer essa ação? Isso não pode ser desfeito!';
$l['no_bio'] = "Nenhuma Bio configurada";
$l['default'] = "Padrão";
$l['follow_all_subcats'] = "Seguir todas as subcategorias dessa categoria?";
$l['upload_image'] = "Enviar uma imagem";
$l['error_users_only'] = "Essa página/ação está apenas disponível para usuários logados. Logue para continuar.";
$l['plans'] = "Planos"; 
$l['until_time'] = "Até {1}"; 
$l['signin_through_steam'] = "Logue atráves do Steam"; 
$l['last_pm_notice'] = 'Você tem uma mensagem privada do <a href="/user/{1}">{2}</a> com assunto <a href="/message/{3}">{4}</a>'; 
$l['private_profile_notice'] = 'Esse perfil é privado'; 
$l['year'] = 'ano'; 
$l['years'] = 'anos'; 
$l['month'] = 'mês'; 
$l['months'] = 'meses'; 
$l['day'] = 'dia'; 
$l['days'] = 'dias';
$l['week'] = 'semana'; 
$l['weeks'] = 'semanas';
$l['hour'] = 'hora'; 
$l['hours'] = 'horas';
$l['minute'] = 'minuto'; 
$l['minutes'] = 'minutos';
$l['second'] = 'segundo'; 
$l['seconds'] = 'segundos';
$l['timeago'] = '{1} {2} atrás'; //{1} = Number {2} = Years/months/days/hour/minutes/seconds
$l['nothing_selected'] = 'Nada selecionado';
$l['no_results'] = 'Nenhum resultado {0}';
$l['size_yb'] = "YB";
$l['size_zb'] = "ZB";
$l['size_eb'] = "EB";
$l['size_pb'] = "PB";
$l['size_tb'] = "TB";
$l['size_gb'] = "GB";
$l['size_mb'] = "MB";
$l['size_kb'] = "KB";
$l['size_b'] = "B";
$l['drop_files'] = "Solte os arquivos aqui para enviar";
$l['file_too_big'] = "Arquivo muito grande ({{filesize}}MiB). Tamanho máximo: {{maxFilesize}}MiB.";
$l['invalid_file_type'] = "Você não pode enviar arquivos desse tipo.";
$l['server_response_error'] = "Servidor respondido com código {{statusCode}}.";
$l['cancel_upload'] = "Cancelar o envio";
$l['cancel_upload_confirm'] = "Tem certeza que deseja cancelar o envio?";
$l['upload_canceled'] = "Envio cancelado";
$l['max_files_exceeded'] = "Você não pode enviar mais os arquivos.";
$l['currently_selected'] = "Atualmente selecionado";
$l['select_and_type'] = "Selecionar e começar a digitar";
$l['select_error'] = "Não foi possível em recuperar os resultados";
$l['select_query'] = "Começa a digitar uma entrada de pesquisa";
$l['searching'] = "Pesquisando";
$l['select_more_char'] = "Por favor coloque mais letras";
$l['blue'] = "Azul";
$l['green'] = "Verde";
$l['pink'] = "Rosa";
$l['red'] = "Vermelho";
$l['teal'] = "Verde-Azulado";
$l['purple'] = "Roxo";
$l['gray'] = "Cinza";
$l['orange'] = "Laranja";
$l['cyan'] = "Ciano";
$l['error_cant_reply'] = "Não pode responder para essa mensagem";
$l['error_no_message'] = "Mensagem não existe";
$l['error_no_user'] = "Usuário não existe";
$l['error_too_quick'] = "Pedido chamado rápido demais";
$l['error_invalid_user'] = "Usuário inválido";
$l['error_banned_message'] = "Usuários banidos não podem enviar mensagens privadas";
$l['error_banned_report'] = "Usuários banidos não podem denunciar mods!";
$l['error_commnet_too_long'] = "Comentário muito grande!";
$l['error_delete_comment'] = "O comentário não existe ou já foi deletado";
$l['error_banned_submit'] = "Usuários banidos não podem enviar mods!";
$l['error_banned_files'] = "Usuários banidos não podem atualizar arquivos!";
$l['error_image'] = "Essa imagem não existe!";
$l['error_transfer_self'] = "Você não pode transferir o seu própio mod para si mesmo!";
$l['error_no_collabs'] = "Esse mod não tem nenhum colaboradores!";
$l['error_file_size'] = "O arquivo é muito grande!";
$l['error_invalid_did'] = "Inválido deu";
$l['error_mod_save'] = "Fracassado em salvar o mod: {0}";
$l['error_mod_create'] = "Fracassado em criar o mod: {0}";
$l['error_mod_no_files'] = "O mod não tem nenhum arquivo e não pode ser acesso pelo público.";
$l['error_mod_waiting'] = "O mod está esperando pela aprovação.";
$l['error_moderator_page'] = "Essa página é só para moderadores!";
$l['error_404'] = "Essa página não existe";
$l['registration_date'] = "Data de Registração";
$l['lastvisit'] = "Última vez visitado";
